// File used to prevent generating routes for content files which do not describe pages
// This is done to avoid errors during yarn generate

// eslint-disable-next-line import/no-default-export
export default [
  'content/free-trial-cta.yml',
  'content/next-step.yml',
  'content/navigation.yml',
  'content/footer.yml',
  'content/apps.yml',
  'content/synced/categories.yml',
  'content/synced/features.yml',
  'content/competition/base_template.yml',
  'content/competition/competitor_template.yml',
  // Sunsetted pages we keep to turn on when needed
  'content/livestream.yml',
  'content/partners/new.yml',
  // Landing pages:
  // These pages have children pages already migrated but an error pops up in the console as nuxt expect the parent to exist
  // These exceptions should be removed when migrating the landing page.
  'content/install',
  'content/solutions',
  'content/stages-devops-lifecycle',
  'content/company/culture',
  'content/pricing/self-managed',
  'content/calculator',
  'content/events',
  'content/synced',
  'content/events',
  'content/features/related-data.yml',
  'content/features/features-cta-section.yml',
];
