## [1.3.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.2.0...v1.3.0) (2023-02-17)


### 🗂️ Content change

* **partners:** Add more technology partners ([7979971](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7979971a4e29e04df7089a3c7ef9cd110406b818))


### ♻️ Refactors

* **carousel:** switch to using v-show and aria improvements ([60ba9c6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/60ba9c60cfcc8aa6d4eaed8d50ffae4db6eb0aee))
* Create reusable categories table component ([e0a665d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e0a665d117b41c9333c238d076b189c0f76c0977))
* Remove references to pps_aggregate ([76eae19](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/76eae192f24d084ea41b3ede4e1c5fed9037aa90))


### 🎨 Styling

* **branding:** DevOps to DevSecOps Tier 1 ([bc8dcc0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bc8dcc0528eb046eaaeacf5b70f11b6537d41778))
* **branding:** DevOps to DevSecOps Tier 1 ([e635224](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e6352243c418311f40de698553e2fcd333e834bd))
* **devsecops:** changing branding from devops to devsecops ([f76b843](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f76b843c83c3ae84e2c14c430c7edc8e99cf18d1))
* **devsecops:** changing branding from devops to devsecops ([b1d8148](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b1d8148d2f280d0915550669b937024a15f0097b))
* **devsecops:** changing branding from devops to devsecops ([5887184](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/58871845a916fbef3d89e73f32384716f9f8209a))


### 🚀 Features

* add methodology pop-up on DevOps Tools ([57cebbb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/57cebbb49bd13b1e9e2e81f89b27dbe1d7f7393a)), closes [#1857](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1857)
* Add the search event listener to integrate the navigation and the BE search ([99248af](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/99248afae765ac10a63e95e8daa6ee69e07e80b1))
* Add the search event listener to integrate the navigation and the BE search ([72107dd](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/72107dda306528b00c7372558c66a20352464946))
* make entire surface area link to case study ([fc54e97](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fc54e978d2a108202831f584cd6626654bcee243)), closes [#1889](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1889)
* **nav release:** Navigation Release Version 3.1.0 ([80ccb35](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/80ccb3572fd324a439f420f2af19f54e099ff91a))
* populate why gitlab feature form using categories file ([26d464c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/26d464c7dd153defef6eb878341ad1128e4665f7))


### 🐜 Bug Fixes

* Make resource card images proportionally consistent when resizing ([30c82af](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/30c82afe02d5537003e5ae10c1858fab3f44d051)), closes [#1891](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1891)
* platform table link and order issues ([df6bb08](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/df6bb08d9a0d2a92faac17aba6995c720fb13cf6))
* wrong URL in open source partners section ([4896b0c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4896b0cc97ce1c88c853d288f5edda2d7d92a22f)), closes [#1925](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1925)

## [1.2.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.1.0...v1.2.0) (2023-02-10)


### ♻️ Refactors

* new engineering issue template ([e26bf48](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e26bf48938f891882c2a2ebbfee3295577bff6c4))


### 🐜 Bug Fixes

* add marketo script to pricing template pages ([01e4e00](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/01e4e009f752b246e7be6e8b83ddccb57cb5e29b))
* analyst page typography ([7433916](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/74339161fdb0b02d7a88a4ba3406d44358852ab1))
* remove unused images - part 2 ([f9db106](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f9db10623e1ce924a3407d6bfea3362f1fa2e7ec))
* **safari:** devops tools table hover state lag ([12f8d67](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/12f8d679ad9874f987f05913d246222a29fa451f)), closes [#1795](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1795)
* **solutions:** Hackerone customer logo distorted in solutions/dev-sec-ops ([0f505bb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0f505bbebe5888a08a384ae00f279b056ccb6302)), closes [#1847](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1847)
* Style changes to devops tools ([3b63c45](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3b63c451f6ef9c4df2d98b058f8788a571bd6647)), closes [#1850](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1850)
* **support links:** updated link for Instance Migration on Statement of Support page ([9e5496e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9e5496e6cd3c16cfcefd616365033cb4a8c97643))
* youtube video embeds on ci solutions page ([5dc805b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5dc805b7585e65b257a0804c8898ecf5c0ec9a27))


### 🚀 Features

* Added an event for Radovan Bacovic as a speaker ([4c1c847](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4c1c8474e992ef965ddc082a89cd806a64bb60ca))
* **competition:** change table title based on stage ([09a4fa5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/09a4fa5387bf13fde508d63e2fe0ae0062864d29)), closes [#1846](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1846)
* create showMore mixin + add showMore functionality to pricing templates ([b9d5702](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b9d57025dc628871522f92d52e5d4203740a719d))
* Make Search Results a New Page ([0ec06a0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0ec06a0f58d73d4b8bf0956a42e0eec25c4d6e8b))
* solutions/open-source page refresh ([d795d7b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d795d7ba6d266c00f03034925ce8e84f00b4e505)), closes [#1837](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1837)
* solutions/open-source/join page refresh ([9654d1f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9654d1f271be7b762306902a76108dedcbf955f1)), closes [#1840](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1840)

## [1.1.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.0.0...v1.1.0) (2023-02-03)


### ♻️ Refactors

* Analyst mvc2 ([720a697](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/720a697f32b85889820ccfb34a19474839950a92))
* **AOS:** Remove AOS in competition pages ([f82fd6b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f82fd6b964bf081279183170d647beaf3f329583))
* **AOS:** Remove AOS in partner pages ([03e860c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/03e860c77c146c23d0d9298c9fddafeded635270))
* **AOS:** Remove AOS in remaining pages ([870c045](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/870c045afdac2f68677370f04480c731ba5f09dc))
* **AOS:** Remove AOS in topic pages ([cf73866](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cf738664fc6db977372bdb246ac705be84c94767))
* **AOS:** Remove AOS on events pages ([cd779b4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cd779b4fd2483831963727bfef33e4e7db0f305b))
* **AOS:** Remove AOS on single landing pages ([e150db6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e150db65b4c2a537e48433139da39488cbff47fb))
* **AOS:** Remove AOS on solutions pages ([d89397e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d89397e0c4a3b911c8e9525d3a100c0a44f3316b))
* Change getElementByID to Vue refs ([89f9426](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/89f9426550e584fd4dc96321440967cfef12fa34))
* Create mixin that handles screen resizing ([c9ab601](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c9ab60194dabf9808cb3a3fa27d602619b53e4b5))
* **default-layout:** Add animation initialization to default layout ([f2143d8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f2143d8f1405083d9275ca19418ac04a9d9130cc))
* Remove all redundant measure units in css files ([925f262](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/925f262c2d089409d4ccdebf7f70d62a20df518a))
* Remove css invalid elements ([4d3dfa2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4d3dfa2312fa14d67a86cf9a2e0d7cdb661fd389))
* Search and remove unused images part 1 ([fffb988](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fffb988d41536e107b0682e96e28f9a4b2e180e5))


### 🎨 Styling

* add new events to events page ([fe78c08](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fe78c08f16a73b716e92585194e9365b4fcbf636))
* Added col to remove content overlapping ([c55f9b2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c55f9b2bc542e5dbcad2104d8fd44c3797ffadac))
* competition page ios safari css updates ([a2e4481](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a2e4481dcd55733603e1c005a63fb886b78970eb))
* fix border bug on competition table ([5c5c038](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5c5c038cc4e67517ed75455fa2d130126243623a))
* fix customer page filter dropdown sizing ([b30e7ca](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b30e7cab61dcebcc85ea12913a7a4dc264401bc4))
* fix horizontal scroll on select pages ([0292641](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/02926410ee29c3e4bc148a863e1606e38235ccdf))
* fix quote carousel background bug ([1ecae17](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1ecae178879b5b9af7cacf9801068c420d3ea981))
* force tier pills to stay on same line ([801a8b1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/801a8b132d76f9f78bb311b762248db7cc688822))
* **get-started:** fix typography and spacing ([3163321](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3163321818982c608db1ab8639917d98b2dda2f2))
* pricing page ios safari css fixes ([32387ea](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/32387eaeb715ecdc2cab39107accbe78d9c834bc))
* styling updates for pricing template pages ([b1c0f32](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b1c0f3237beb6648619391743890906ad2910ae4))


### 🚀 Features

* Add buttons to find a partner ([c1d636a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c1d636a91f51362344007ae9950a51c57d1d780f))
* Add clarity on limits to pricing and trial page ([13318a6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/13318a6de0e4cdeac540f5a80900e7c8a0f3ca0d))
* Add Deakin University case study ([5d91007](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5d91007e58f2d9d5a3b67e610de75a6912594804))
* Add deep link into tabs on /competition page ([35022b7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/35022b7bfdbb986a5ccb51fe96e4cc212236b618)), closes [#1770](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1770)
* Add link to GitLab vs pop-ups ([ff0fea8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ff0fea836e41add08c1f72227a3167fd542c6fd2)), closes [#1733](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1733)
* add new report to CAP page ([e9ad7ed](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e9ad7ed325dc256353c9301b86d979b112897825))
* add photo to GitLab Dedicated page ([b21b8f7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b21b8f7e6341e3d3d7abd90ee6436951c10d3eb9)), closes [#1707](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1707)
* add roi calculator abc test ([9d35776](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9d35776c310d7145bbb9cdb31a402c5d75f7716d))
* Add VSD sign-up page ([48988ef](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/48988ef8f8f391a057aa7defef2ed892229bc56d)), closes [#1800](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1800)
* Added 2 events for Radovan Bacovic as a speaker ([b89eaf3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b89eaf3dd496714606d3db8fb2dbacdf42092132))
* Adding Notion, Firefly, Gitbook & AnalyticsVerse to alliance partners ([7c6f215](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7c6f215d512cd18c1c276ebb28ae883302b2056c))
* Adding Tech Talk: GitLab for Governance and Compliance to the events page ([3c18a79](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3c18a79a5a2732b9bdfecae379f5700ec14f115d))
* Build business case for GitLab checklist ([bcc5dd6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bcc5dd643017da91a2807c65c1fa910b564e9488))
* **competition:** Add a hyperlink to DevOpsTools ([39513a7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/39513a77558ee8981fd1d5b47fad8f6dd2829549)), closes [#1848](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1848)
* Create brand campaign landing page ([e1e8935](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e1e893557ab39ac231de49edddfcf1a60b78176f))
* Create changelog scheduled job to run it only on "main" ([b1eddcb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b1eddcbd766baeea9905490de52a61f3ed4a5523))
* create digital transformation topics page ([6a7f6d8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6a7f6d83db7d8043b13fa17cb2a63c3213e3f88e))
* Create Direct Link to Pricing Page FAQs ([40ccd01](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/40ccd01504e9693a633e0429db63d9bc62b37154)), closes [#635](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/635)
* Deep link from /devops-tool stage to /competition tab" ([36c238e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/36c238eca27f636d101cea8904e426f99d65bc63)), closes [#1773](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1773) [#1772](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1772)
* Education solution page ([21eafd4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/21eafd4bc5331bebed60bf09fce5bf5235524506))
* **nav release:** DEX upgrade be-navigation to 2.2.0 ([b16a814](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b16a81461c5d8b397fcc52a8a6dd3942ad17dc82))
* **nav:** Nav 3.0.2 release ([549a01e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/549a01e9bab1a3aacbb4e416cd867a86523c4e7f))
* new UX for competition VS pages ([4489a29](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4489a2966511e5d3c2920389efd1907da3324296))
* Refresh TeamOps landing page with content and verbiage updates ([15b60e9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/15b60e9d61788f9c67475531c426ae3fabcf176a))
* Releasing support for notifications in the Slack app. ([9d5d342](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9d5d3420ca92e54771891bf0cc61479b59d67c13))
* remove ROI cta for same-page redirects ([3c8bcc5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3c8bcc500b66e42a2bf6b8213bcd3ecf9f808d0c))
* sync platform page table up with categories.yml file ([0993613](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/09936132264ee045d7d3157744a25f597427cf60))
* update competition page UX ([2ad64ea](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2ad64ea69fe472bf7d924476acb6f4208aeed048))


### 🐜 Bug Fixes

* 404 links ([7d9832b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7d9832b87011a2cb3777b7f43488623611317244))
* add text to product analytics card ([799b329](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/799b3297871d1bfe745468f0d3b504280944cb6c))
* brand campaign devsecops image ([be5bc90](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/be5bc90d12049357ad805e7eb50f817203872535))
* change date for webcast ([f31aed9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f31aed9fc31ff2b67bee2a8a779a64fdd1bdae81))
* content: Remove "API Clients" and "CLI Clients" from the technology-partners ([e629a59](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e629a59713bc2307503fd64f3bc15fc7c1a522e8))
* Enterprise and SMB Page Loading Slow ([6528680](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6528680f5a77f81cfbd0bffccc922bb5ad9b5648))
* **gtm/ot:** unblocks gtm from ot ([1b42a3e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1b42a3e72252aaa0f1c0208ae8ffad0b4ade44cc))
* **gtm:** removing optanon class ([cf31330](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cf3133098911d06a4bdb571cecc78812b8f356f0))
* info for course to remove duplicate option ([beec9f8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/beec9f858c12d9dc13476a3371353d0facbde43f))
* Navigation increment v2.1.1 ([36b2e70](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/36b2e703c80669c5057a998fc50c4b017b19e5e3))
* **OT/GTM:** removing optanon class from GTM ([fa24bdf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fa24bdf84dce2f46fe6a04a6cdd7670d51617041))
* Project Dependency List on Feature Comparison page ([56e03e8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/56e03e8a40916b5111cbb9e7a6981dfc9dd7eb95))
* Quote block layout on mobile ([5437995](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/54379957c9883d2a17abebbf268ae6547d480833)), closes [#1810](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1810)
* random quotation mark on /features/continuous-integration/ ([0712c32](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0712c32f8e6963e48c1789c43668fbccc981d44c)), closes [#1811](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1811)
* remove entry from customers page that leads to 404 ([3d4af09](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3d4af094950bac67c8bc70686702301ddfa3b250))
* Rename side-navigation, improve reliability of scrolling events ([9c9a96e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9c9a96e4c577f5e815a782c60f576eecf465d922)), closes [#1398](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1398)
* Replace all instances of all-caps with title-case ([c223413](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c223413a75ab06f2f33f07c3eaa5f20321669734)), closes [#1563](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1563)
* Resolve "Removing partners without TPAs" ([9810ab5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9810ab5199b2eceac726b2bec0968e633eaaa419))
* **sidenav:** Source code management components displaying incorrectly ([3b1635d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3b1635dedcb391c61cd32e1573e1a6ab74368ed9)), closes [#1794](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1794)
* **sidenavvariant:** now takes up space in dom, updated relevant yml files ([e9e9225](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e9e9225ba021a11760eb30656b9a859511d489e5)), closes [#1572](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1572)
* sitemap with 2 paths to exclude ([c30ccc4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c30ccc4c73c48e5d4811a128916704de17494474))
* SlpBreadcrumb on Topics Pages ([7faa2d5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7faa2d5e259bafa23c2b93e66d0b97929c029ede)), closes [#1780](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1780)
* Small business page components have extra spacing from the sidenav ([2651843](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2651843311414fda90532298e3cb136216cbb740)), closes [#1813](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1813)
* software faster vimeo analytics are not being tracked ([3c0d01f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3c0d01f5db45872dbf79b07f4b41f8f886ee1e4e))
* style of pricing button to remove extra space ([d61bca5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d61bca56de0ca4776d45f2905d8df2d6cfdf54e2))
* transparent icon on solution page card ([e820d49](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e820d496336b5938554112a93ead6fba092808da))
* typo in pricing faq ([a7d60eb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a7d60ebe8547c4cff7ac610742b4ea85e826dbf3))
* typo on gitops page ([6e3c13d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6e3c13dc26c42db892832852f8b0c101dcffac3c))
* typo on the aws-reinvent page ([553bc48](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/553bc48bcf72812d6fcd59d7ec30489a7d5eb290))
* Update colors on the TeamOps page ([f528946](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f528946dff4b5e2d2b4fbaebb6d458d1de93472e)), closes [#1753](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1753)
* update dedicated page url ([b8dfaf8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b8dfaf8320197ef508554383dc44acc5e22a2790))
* update url for product analytics docs ([959204f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/959204f61d5b3a2c703ab6e54377f7e3841bea99))
* Updates to VSM pages, Add DORA page links ([6c420f6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6c420f6dc667eb3080cafccb7e204c44f3c6487f)), closes [#1650](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1650)


### 🗂️ Content change

* Add Contributor Days Event ([a086b47](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a086b475bf33d030edb055f46868d8d245a1cbb0))
* **technology-partners:** Reclassification of technology partners ([9360c7e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9360c7e1283668eab27d6fcdb1f9901ef02f541e))
