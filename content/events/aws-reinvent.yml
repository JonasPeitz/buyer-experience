metadata:
  title: GitLab + AWS re:Invent 2022
  og_itle: GitLab + AWS re:Invent 2022
  description: "We can’t wait to see you all in person again for five incredible days of AWS re:Invent! The GitLab team is ready."
  twitter_description: "We can’t wait to see you all in person again for five incredible days of AWS re:Invent! The GitLab team is ready."
  image_title: /nuxt-images/open-graph/open-graph-aws-reinvent.png
  og_description: "We can’t wait to see you all in person again for five incredible days of AWS re:Invent! The GitLab team is ready."
  og_image: /nuxt-images/open-graph/open-graph-aws-reinvent.png
  twitter_image: /nuxt-images/open-graph/open-graph-aws-reinvent.png

hero:
  headline: Join GitLab at AWS re:Invent in Las Vegas
  accent: GitLab + AWS re:Invent 2022
  sub_heading: Join the GitLab team to…
  list:
  - icon: bulb-bolt-black-sm.svg
    description: Attend exciting lightning talks at our booth!
  - icon: board-black.svg
    description: Listen to an engaging speaking session with NASDAQ
  - icon: celebrate-black-sm.svg
    description: Throw darts, network, enjoy amazing drink and food, and listen to DJ Graffiti at our evening event at Flight Club
  - icon: tee-black.svg
    description: Get exclusive GitLab swag
  call_to_action:
    button_text: Book a meeting
    button_link: https://calendly.com/greib/aws-reinvent-2022-demo-or-meeting-with-gitlab

intro:
  headline: 'Come say ‘hi’ at the GitLab booth'
  description: 'Our DevOps experts are on hand to answer all of your questions.'

gitlab_booth:
  information: "Stop by the GitLab Booth #228 and talk to one of our many DevOps experts and see The One DevOps Platform in action! Dive into new capabilities, learn best practices, and get answers to all of your technical questions. Share your feedback and let us know what you'd like to see in the GitLab platform! And that’s not all!: We’ll have an exciting lineup of action-packed lightning talks happening all week in the booth."
  subheadings:
    - title: Booth
      value: 228
  image:
    src: "gitlab-booth.jpeg"
    alt: ''

party:
  title: DevOps & Darts
  image:
    src: "devops-darts-social-promo-1200x628.png"
    alt: ''
  subheadings:
  - title: Date
    value: Wednesday, November 30, 2022
    icon: calendar-purple.svg
  - title: Time
    value: 6:30-9:30PM
    icon: stopwatch-purple.svg
  - title: Location
    value: 'Grand Canal Shoppes at The Venetian Resort®️ 3327 South Las Vegas BLVD, Space #2720'
    icon: pin-purple.svg
  information: Throw darts, talk DevOps, and enjoy tasty food and drink while DJ Graffiti spins at GitLab’s evening event at Flight Club.
  call_to_action:
    button_text: Join the party!
    button_link: https://www.eventbrite.com/e/aws-reinvent-2022-happy-hour-devops-darts-at-flight-club-tickets-429954875607
  sponsors:
    - 'vmw-prod-icon-tanzu-web_horiz 1.svg'
    - 'Vector (2).svg'
    - 'sysdig-open-source-logo 1 (2).svg'
    - 'Mezmo_Logo_White_L.png'
    - 'JupiterOne.svg'
    - 'horizontal-light.png'

speaker_session:
  title: Building an Onboarding COE for AWS - Going Beyond Automation
  description: What is an onboarding COE? How do you go beyond automation? Cloud Migrations and App Modernization projects are critical to how we adopt the cloud as a runtime platform rather than a bunch of disparate services.
  list:
  - title: Time
    value: Thursday, December 1 at 11:45 AM
  - title: Location
    value: MGM Room 115
  body: >
    Using GitLab's unique platform capabilities, we will show you how Nasdaq is building onboarding templates to provide provisioning, deployment, release and security best practices for day 1 activities.

    You’ll walk away from this session with knowledge about how highly regulated companies implement SplatOps techniques for auditing and compliance.
  speakers:
    - name: "Amado Gramajo"
      title: "VP, Observability, Infrastructure, & DevOps Engineering"
      company: "Nasdaq"
      image: "aws-reinvent/Amado_Gramajo.jpeg"
    - name: "Lee Faus"
      title: "Global Field CTO"
      company: "GitLab"
      image: "aws-reinvent/Lee_Faus.jpeg"

schedule:
  title: Live Demos & Lightning Talks
  description: Get a deeper dive into the GitLab platform in our booth
  body:
  - Swing by the booth for a look at the latest GitLab features during a live demo, or check out one of our expert lightning talks - happening every hour!
  - Come by the booth to watch a live demo or attend a lightning talk happening every hour!
  schedule_list:
    - date: Monday, Nov 28
      events:
      - title: Extinguishing Dumpster Fires with GitLab since 2014
        speaker: Sarah Bailey
      - title: Advanced Application Security and Vulnerability Management with GitLab DevSecOps
        speaker: Fernando Diaz
      - title: AWS re:Invent - Partner Lightning Talk
        speaker: Kevin Krider and Forester Vosburgh, Solutions Architect at BoxBoat
      - title: "DORA Metrics: keeping track of your teams' efficiency"
        speaker: Cesar Saavedra
      - title: Value stream management
        speaker: Itzik Gan-Baruch
    - date: Tuesday, Nov 29
      events:
      - title: Efficient DevSecOps Pipelines in a Cloud Native World
        speaker: Fernando Diaz
      - title: "DORA Metrics: keeping track of your teams' efficiency"
        speaker: Cesar Saavedra
      - title: Take Action Easily with GitLab Issues
        speaker: Sophia Manicor
      - title: GitLab CI overview
        speaker: Itzik Gan-Baruch
      - title: Advanced Application Security and Vulnerability Management with GitLab DevSecOps
        speaker: Fernando Diaz
      - title: "Connect the world of DevOps to work in ServiceNow"
        speaker: Usman Sindhu, Product Marketing Director at ServiceNow
      - title: Beginners Guide to AWS and GitLab
        speaker: PJ Metz
      - title: CI/CD at scale with GitLab Runners on EKS
        speaker: Kevin Krider and Forester Vosburgh, Solutions Architect at BoxBoat
      - title: Optimizing response time with PagerDuty + GitLab
        speaker: Joint talk with PagerDuty
      - title: "GitOps: the GitLab Agent for Kubernetes"
        speaker: Cesar Saavedra
      - title: The One DevOps Platform for Machine Learning Workloads
        speaker: William Arias
    - date: Wednesday, Nov 30
      events:
      - title: Advanced Application Security and Vulnerability Management with GitLab DevSecOps
        speaker: Fernando Diaz
      - title: Beginners Guide to AWS and GitLab
        speaker: PJ Metz
      - title: Extinguishing Dumpster Fires with GitLab since 2014
        speaker: Sarah Bailey
      - title: AI for your code!
        speaker: Brandon Jung, General Manager at Tabnine
      - title: Merging Learning Into Your DevSecOps Workflows
        speaker: Trevor Hess, Director, Technical Marketing at Secure Code Warrior
      - title: Moving Clients to GitLab Platform to enable digital transformation
        speaker: 'Doug Garaux, Vice President: DevOps and Larry Grant, Field CTO, Cloud Advisor at Presidio'
      - title: Refactor Readiness
        speaker: Lee Faus
      - title: Value stream management
        speaker: Itzik Gan-Baruch
      - title: Building a better contributor experience
        speaker: John Coghlan
      - title: The One DevOps Platform for Machine Learning Workloads
        speaker: William Arias
      - title: "DORA Metrics: keeping track of your teams' efficiency"
        speaker: Cesar Saavedra
    - date: Thursday, Dec 1
      events:
      - title: Take Action Easily with GitLab Issues
        speaker: Sophia Manicor
      - title: "Progressive Delivery: Feature Flags"
        speaker: Cesar Saavedra
      - title: GitLab CI overview
        speaker: Itzik Gan-Baruch
      - title: Building a better contributor experience
        speaker: John Coghlan
      - title: Extinguishing Dumpster Fires with GitLab since 2014
        speaker: Sarah Bailey
      - title: The One DevOps Platform for Machine Learning Workloads
        speaker: William Arias
      - title: Efficient DevSecOps Pipelines in a Cloud Native World
        speaker: Fernando Diaz
      - title: Code Studio - The world's only full stack Drupal development platform built on GitLab
        speaker: Stephen Raghunath, Director of Cloud Engineering at Acquia

