---
  title: GitLab for Open Source
  description: Open source communities benefit from The DevSecOps Platform.
  image_title: /nuxt-images/open-graph/open-source-card.png
  image_alt: GitLab for Open Source
  twitter_image: /nuxt-images/open-graph/open-source-card.png
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab Solutions for Open Source Projects
        subtitle: Create. Configure. Monitor. Secure. Open source communities benefit from The DevSecOps Platform.
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: /solutions/open-source/join/
          text: Join the program
          data_ga_name: join open source program
          data_ga_location: header
        secondary_btn:
          url: https://forum.gitlab.com/c/community/gitlab-for-open-source/49?_gl=1*ydz9eq*_ga*NDg4NTEwMzgxLjE2NzQ3NDk1MTk.*_ga_ENFH3X7M5Y*MTY3NDgzOTk4NC41LjEuMTY3NDg0MTU1Ny4wLjAuMA..
          text: Ask a question
          data_ga_name: ask an open source question
          data_ga_location: header
        image:
          image_url: /nuxt-images/open-source/open-source-hero.png
          alt: "Image: gitlab for open source"
          bordered: true
    - name: 'side-navigation-variant'
      links:
        - title: Benefits
          href: '#benefits'
        - title: Testimonials
          href: '#testimonials'
        - title: Partners
          href: '#partners'
        - title: Case Studies
          href: '#case-studies'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'benefits'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: Why GitLab for open source?
                light_background: true
                feature:
                  title: Get started with GitLab for Open Source
                  description: Unlock your community's full potential. Features of <a href="/pricing/ultimate/">GitLab Ultimate</a>—including 50,000 CI/CD minutes—are free to qualifying open source projects through the GitLab for Open Source Program.
                  link: 
                    href: /solutions/open-source/join/
                    text: Join the program
                  image: /nuxt-images/open-source/open-source-feature.png
                  alt: crowd during a presentation
                cards:
                  - title: We're open
                    description: GitLab's <a href="https://gitlab.com/gitlab-org">open core </a> is published under an MIT open source license. The rest is source-available. <a href="/community/contribute/" >Everyone can contribute</a> to making GitLab better. View our <a href="https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=start_date_asc&layout=WEEKS&timeframe_range_type=CURRENT_QUARTER&progress=WEIGHT&show_progress=true&show_milestones=true&milestones_type=ALL">transparent roadmap </a>and propose features your project needs.
                    icon:
                      name: merge
                      alt: merge Icon
                      variant: marketing
                  - title: You're in control
                    description: Download, install, and manage <a href="/install/?version=ce">your own GitLab instance</a>. Or <a href="/pricing/">let us</a>—or <a href="https://partners.gitlab.com/English/directory/">a partner</a>—do it for you. Deploy to the cloud you prefer. With GitLab, you have <a href="/install/ce-or-ee/">options</a>.
                    icon:
                      name: microservices-cog
                      alt: microservices-cog Icon
                      variant: marketing
                  - title: Community powered
                    description: Connect with open source enthusiasts on the <a href="https://forum.gitlab.com/c/community/gitlab-for-open-source/49">GitLab Forum</a> to find support and collaborate. And meet members of the <a href="/solutions/open-source/partners/">GitLab Open Source Partners</a> program to learn how large-scale open source projects innovate with GitLab.
                    icon:
                      name: community
                      alt: community Icon
                      variant: marketing
                  - title: Built for collaboration
                    description: Your entire community can use GitLab—not only developers. Onboard new members with ease. Foster cross-team collaboration. Maintain your documentation. Plan new features and track upcoming milestones. Automate testing. GitLab's <a href="/stages-devops-lifecycle/">end-to-end platform</a> helps everyone contribute.
                    icon:
                      name: collaboration-alt-4
                      alt: Collaboration Icon
                      variant: marketing
        - name: 'div'
          id: testimonials
          slot_enabled: true
          slot_content: 
            - name: 'by-industry-quotes-carousel'
              data:
                header: |
                  Enterprises trust it. 
                  <br />
                  Open source communities love it.
                quotes:
                  - title_img:
                      url: /nuxt-images/logos/drupal_mono.png
                      alt: Drupal logo
                    quote: The ability to contribute without setting up complex local development environments has made it easier for everyone from our accessibility maintainers, documentation editors, product managers, and others to review and comment on the work of our developers.
                    author: Timothy Lehnen
                    position: Drupal Association
                    ga_carousel: open source drupal
                    url: /customers/drupalassociation/
                  - title_img:
                      url: /nuxt-images/organizations/logo_kde_color.svg
                      alt: University of Washington Logo
                    quote: Adopting GitLab has been a natural next step for us. Being able to allow project contributors to easily participate in how the products they maintain are tested and delivered will certainly be a turning point for our ecosystem.
                    author: Alex Pol
                    position: KDE e.V
                    ga_carousel: open source kde
                    url: /blog/2020/06/29/welcome-kde/
        - name: 'div'
          id: partners
          slot_enabled: true
          slot_content:
            - name: open-source-partners-grid
              data: 
                header: Our open source partners
                description: GitLab Open Source Partners are building the future of open source on GitLab.
                link: /solutions/open-source/partners/
                partners:
                  - logo: /nuxt-images/open-source/gnome.png
                    alt: gnome Logo
                  - logo: /nuxt-images/open-source/KDE.png
                    alt: KDE Logo
                  - logo: /nuxt-images/open-source/drupal.png
                    alt: drupal Logo
                  - logo: /nuxt-images/open-source/finos.png
                    alt: finos Logo
                  - logo: /nuxt-images/open-source/arm.png
                    alt: arm Logo
                  - logo: /nuxt-images/open-source/ska.png
                    alt: ska Logo
                  - logo: /nuxt-images/open-source/vlc.png
                    alt: ska Logo
                  - logo: /nuxt-images/open-source/debian.png
                    alt: ska Logo
                  - logo: /nuxt-images/open-source/centos.png
                    alt: ska Logo
                  
        - name: 'div'
          id: 'case-studies'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-case-studies'
              data:
                charcoal_bg: true
                title: Open source success stories
                rows:
                  - title: Drupal
                    subtitle: Drupal Association eases entry for new committers, speeds implementations
                    image:
                      url: /nuxt-images/open-source/drupalassoc_cover.jpeg
                      alt: Person working on computer
                    button:
                      href: /customers/drupalassociation/
                      text: Learn more
                      data_ga_name: drupal learn more
                      data_ga_location: case studies
                  - title: Square Kilometre Array
                    subtitle: How SKA uses GitLab to help construct the world’s largest telescope
                    image:
                      url: /nuxt-images/open-source/ska-cover.jpeg
                      alt: Antena array
                    button:
                      href: /customers/square_kilometre_array/#
                      text: Learn more
                      data_ga_name: SKA learn more
                      data_ga_location: case studies
                  - title: Synchrotron Soleil
                    subtitle: GitLab accelerates innovation and improves efficiency for Synchrotron SOLEIL
                    image:
                      url: /nuxt-images/open-source/synchro_soleil_cover.jpeg
                      alt: Synchrotron Soleil
                    button:
                      href: /customers/synchrotron_soleil/#
                      text: Learn more
                      data_ga_name: uw learn more
                      data_ga_location: case studies
    - name: solutions-cards
      data:
        title: Explore other ways GitLab can help open source developers
        column_size: 4
        link :
          url: /solutions/
          text: Explore Solutions
          data_ga_name: soluions explore more
          data_ga_location: body
        cards:
          - title: Source Code Management
            description: GitLab makes Source Code Management easy
            icon:
              name: cog-code
              alt: cog-code Icon
              variant: marketing
            href: /stages-devops-lifecycle/source-code-management/
            data_ga_name: devsecpps learn more
            data_ga_location: body
          - title: Continuous Integration and Delivery
            description: Make software delivery repeatable and on-demand
            icon:
              name: continuous-delivery
              alt: Continuous Delivery
              variant: marketing
            href: /features/continuous-integration/
            data_ga_name: Continuous Integration and Delivery learn more
            data_ga_location: body
          - title: Continuous Software Security
            description: Shift security left with built-in DevSecOps
            icon:
              name: devsecops
              alt: devsecops Icon
              variant: marketing
            href: /solutions/continuous-software-security-assurance/
            data_ga_name: Continuous Software Security learn more
            data_ga_location: body

