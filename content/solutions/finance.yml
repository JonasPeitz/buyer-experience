---
  title: GitLab for financial services
  description: Social coding, continuous integration, and release automation have proven to accelerate development and software quality to meet mission objectives. Learn more!
  twitter_image: /nuxt-images/open-graph/Opengraph-GitLab-finances.png
  og_image: /nuxt-images/open-graph/Opengraph-GitLab-finances.png
  image_title: /nuxt-images/open-graph/Opengraph-GitLab-finances.png
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab for financial services
        subtitle: The DevSecOps platform for innovative financial institutions
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Try Ultimate for Free
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Learn about pricing
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/finance.jpeg
          image_url_mobile: /nuxt-images/solutions/finance.jpeg
          alt: "Image: GitLab for financial services"
          bordered: true
    - name: 'by-industry-intro'
      data:
        logos:
          - name: Goldman Sachs
            image: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
            url: /customers/goldman-sachs/
            aria_label: Link to Goldman Sachs customer case study
          - name: Bendigo and Adelaide Bank
            image: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
            url: /customers/bab/
            aria_label: Link to Bendigo and Adelaide Bank customer case study
          - name: Credit Agricole
            image: /nuxt-images/customers/credit-agricole-logo-2.png
            url: /customers/credit-agricole/
            aria_label: Link to Credit Agricole customer case study
          - name: Worldline
            image: /nuxt-images/logos/worldline-logo-mono.png
            url: /customers/worldline/
            aria_label: Link to Worldline customer case study
          - name: Moneyfarm
            image: /nuxt-images/logos/moneyfarm_logo.png
            url: /customers/moneyfarm/
            aria_label: Link to Moneyfarm customer case study
          - name: UBS
            image: /nuxt-images/home/logo_ubs_mono.svg
            url: /blog/2021/08/04/ubs-gitlab-devops-platform/
            aria_label: Link to UBS customer case study
    - name: 'side-navigation-variant'
      links:
        - title: Overview
          href: '#overview'
        - title: Testimonials
          href: '#testimonials'
        - title: Capabilities
          href: '#capabilities'
        - title: Benefits
          href: '#benefits'
        - title: Case Studies
          href: '#case-studies'
      slot_enabled: true
      slot_offset: 2
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content: 
            - name: 'by-solution-benefits'
              data:
                title: Security. Speed. Efficiency. 
                is_accordion: false
                items:
                  - icon:
                      name: devsecops
                      alt: Devsecops Icon
                      variant: marketing
                    header: Reduce security and compliance risk
                    text: Detect vulnerabilities and security issues earlier in the development process and enforce policies to adhere to compliance requirements.
                    link_text: Learn more about DevSecOps
                    link_url: /solutions/dev-sec-ops
                    ga_name: reduce security learn more
                    ga_location: benefits
                  - icon:
                      name: increase
                      alt: increase Icon
                      variant: marketing
                    header: Faster time to market
                    text: Improve your customers’ experience and get new features to them faster by automating your software delivery process with a complete DevSecOps platform.
                    link_text: Learn more
                    link_url: /platform/
                    ga_name: free up learn more
                    ga_location: benefits
                  - icon:
                      name: piggy-bank-alt
                      alt: piggy-bank Icon
                      variant: marketing
                    header: Decrease operational costs
                    text: Reduce toolchain complexity and enable greater collaboration, productivity, and innovation with a single solution.
                    link_text: Calculate your savings
                    link_url: /calculator/roi/
        - name: 'div'
          id: 'testimonials'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              offset: 1
              data:
                header: |
                  Trusted by the financial services industry.
                  <br />
                  Loved by developers.
                quotes:
                  - title_img:
                      url: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
                      alt: goldman sachs
                    quote: “GitLab has allowed us to dramatically increase the velocity of development in our Engineering Division… We now see some teams running and merging 1000+ CI feature branch builds a day!"
                    author: Andrew Knight
                    position: Managing Director at Goldman Sachs
                    ga_carousel: financial services goldman sachs
                  - title_img:
                      url: /nuxt-images/home/logo_ubs_mono.svg
                      alt: UBS Logo
                    quote: |
                      "We wouldn't be able to have that seamless experience without GitLab. It allowed us to pull ahead of many of our competitors, and break down the barriers between coding, testing, and deployment."
                    author: Rick Carey
                    position: Group Chief Technology Officer at UBS
                    ga_carousel: financial services UBS
                  - title_img:
                      url: /nuxt-images/logos/new10.svg
                      alt: new10 Logo
                    quote: “GitLab is really helping us in our very modern architecture, because you're supporting Kubernetes, you're supporting serverless, and you are supporting cool security stuff, like DAST and SAST. GitLab is enabling us to have a really cutting edge architecture.”
                    author: Kirill Kolyaskin
                    position: CTO at New10
                    ga_carousel: financial services new10
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              offset: 2
              data:
                subtitle: A complete DevSecOps platform for financial services
                sub_description: 'Help your customers with their financial needs more efficiently and securely with:'
                white_bg: true
                sub_image: /nuxt-images/solutions/dev-sec-ops/dev-sec-ops.png
                alt: showcase image
                solutions:
                  - title: SBOM and dependencies
                    description: Get a clear view of your project’s dependencies or software bill of materials, including any known vulnerabilities and other key details such as the packager that installed each dependency and its software license.
                    icon:
                      name: less-risk
                      alt: Less Risk Icon
                      variant: marketing
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/user/application_security/dependency_list/
                    data_ga_name: sbom
                    data_ga_location: solutions block
                  - title: Comprehensive security scanning
                    description: Detect vulnerabilities and secure your applications with built-in security tools, including SAST, secret detection, container scanning, cluster image scanning, DAST, IaC scanning, API fuzzing, and more.
                    icon:
                      name: monitor-pipeline
                      alt: Monitor Pipeline Icon
                      variant: marketing
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/user/application_security/
                    data_ga_name: Comprehensive security scanning
                    data_ga_location: solutions block
                  - title: Fuzz testing
                    description: Discover bugs and potential security issues that other QA processes may miss by adding coverage-guided fuzz testing to your pipelines in GitLab.
                    icon:
                      name: monitor-test
                      alt: Monitor Test Icon
                      variant: marketing
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing
                    data_ga_name: fuzz testing
                    data_ga_location: solutions block
                  - title: Common controls for compliance 
                    description: Automate and enforce common controls with various GitLab features from protected branches and environments to merge request approvals, audit logs, artifact retention, and comprehensive security scanning.
                    icon:
                      name: shield-check
                      alt: Shield Check Icon
                      variant: marketing
                    link_text: Learn More
                    link_url: https://about.gitlab.com/solutions/financial-services-regulatory-compliance/
                    data_ga_name: controls for compliance
                    data_ga_location: solutions block
                  - title: Compliant workflow automation 
                    description: Ensure the settings your compliance team configures stay configured that way and working correctly with compliance frameworks and pipelines.
                    icon:
                      name: devsecops
                      alt: devsecops Icon
                      variant: marketing
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html#compliant-workflow-automation
                    data_ga_name: common controls for compliance
                    data_ga_location: solutions block
        - name: 'div'
          id: 'benefits'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: Uniquely suited for financial services
                cards:
                  - title: Built-in security scanning 
                    description: Empower developers to find and fix security issues as they are created. Use GitLab’s <a href="https://docs.gitlab.com/ee/user/application_security/">built-in scanners</a> — including SAST, DAST, container scanning, IaC scanning, and secret detection to name a few — and integrate custom scanners.
                    icon:
                      name: monitor-test
                      alt: monitor test Icon
                      variant: marketing
                  - title: Supports your cloud journey
                    description: GitLab is designed for cloud-native applications and has tight integrations with <a href="https://about.gitlab.com/solutions/kubernetes/">Kubernetes</a>, <a href="https://about.gitlab.com/partners/technology-partners/aws/">AWS</a>, <a href="https://about.gitlab.com/partners/technology-partners/google-cloud-platform/" >Google Cloud</a>, and <a href="https://about.gitlab.com/partners/technology-partners/#cloud-partners">other cloud providers</a>. 
                    icon:
                      name: gitlab-cloud
                      alt: gitlab-cloud Icon
                      variant: marketing
                  - title: On-prem, self-hosted or SaaS
                    description: GitLab works in all environments. The choice is yours.
                    icon:
                      name: monitor-gitlab
                      alt: monitor-gitlab Icon
                      variant: marketing
               
        - name: 'div'
          id: 'case-studies'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-case-studies'
              data:
                title: Success stories from the finance industry
                rows:
                  - title: Goldman Sachs
                    subtitle: How dozens of teams at Goldman Sachs are pushing to production in less than 24 hours
                    image:
                      url: /nuxt-images/blogimages/Goldman_Sachs_case_study.jpg
                      alt: buildings from the street
                    button:
                      href: https://about.gitlab.com/customers/goldman-sachs/
                      text: Learn more
                      data_ga_name: goldman sachs learn more
                      data_ga_location: case studies
                  - title: Moneyfarm
                    subtitle: This European wealth management firm improved their developer experience and doubled deployments
                    image:
                      url: /nuxt-images/blogimages/moneyfarm_cover_image_july.jpg
                      alt: reflections in building windows
                    button:
                      href: https://about.gitlab.com/customers/moneyfarm/
                      text: Learn more
                      data_ga_name: uw learn more
                      data_ga_location: case studies
                  - title: Bendigo and Adelaide Bank
                    subtitle: How one of Australia’s most trusted banks embraced cloud technology and improved operational efficiency
                    image:
                      url: /nuxt-images/blogimages/bab_cover_image.jpg
                      alt: black and white buildings
                    button:
                      href: https://about.gitlab.com/customers/bab/
                      text: Learn more
                      data_ga_name: bendigo and adelaide learn more
                      data_ga_location: case studies
    - name: 'by-solution-link'
      data:
        title: 'GitLab Events'
        description: "Join an event to learn how your team can deliver software faster and more efficiently, while strengthening security and compliance. We'll be attending, hosting, and running numerous events in 2022, and can’t wait to see you there!"
        link: /events/
        image: /nuxt-images/events/pubsec-event-link.jpeg
        alt: crowd during a presentation
        icon: calendar-alt-2
