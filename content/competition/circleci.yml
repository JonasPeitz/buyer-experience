# This file is not directly used, but is used as a template for new competition pages.
---
  data:
    competitor: CircleCI
    gitlab_coverage: 75
    competitor_coverage: 25
    subheading: CircleCI is a CI/CD platform for build, test and deploy code by connecting to an external VCS like GitLab, GitHub or BitBucket. It includes tools to improve developer productivity such as Insights and SSH debugging.
    comparison_table:
      - stage: Verify
        features:
          - feature: Continuous Integration (CI) - Pipelines
            gitlab:
              coverage: 100
              projected_coverage: 100
              description: GitLab is a leading edge tool in CI and provides and maintains all the features required for a complete continuous integration solution.
              details: |
                * Customers can easily self-manage Runners on VM's, containers, or physical machines in the cloud or on-premise and use those at the group or project level on GitLab SaaS to run CI jobs
                * A directed acyclic graph can be used in the context of a CI/CD pipeline to build relationships between jobs such that execution is performed in the quickest possible manner, regardless how stages may be set up.
                * Similarly to multi-project pipelines, a pipeline can trigger a set of concurrently running downstream child pipelines, but in the same project: |
                    * Child pipelines still execute each of their jobs according to a stage sequence, but would be free to continue forward through their stages without waiting for unrelated jobs in the parent pipeline to finish.
                    * The configuration is split up into smaller child pipeline configurations. Each child pipeline contains only relevant steps which are easier to understand. This reduces the cognitive load to understand the overall configuration.
                    * Imports are done at the child
                * Use resource groups to strategically control the concurrency of the jobs for optimizing continuous deployments workflow with safety.
                * Use scheduled pipelines to run GitLab CI/CD pipelines at regular intervals.
              improving_product_capabilities: |
                * [Secure CI_JOB_TOKEN Workflows](https://gitlab.com/groups/gitlab-org/-/epics/6546).
                * [Make GitLab CI/CD work better with external repositories](https://gitlab.com/groups/gitlab-org/-/epics/943).
              link_to_documentation: https://docs.gitlab.com/ee/ci/
            competitor:
              coverage: 75
              description: CircleCI is a CI-only tool, you need to connect to the supported VSC providers (GitLab, GitHub, BitBucket); CircleCI doesn’t have built-in security scans, but integrates with third party tools like Snyk.
              details: |
                * Speed-up pipeline execution - users can choose the operating system, CPU, GPU, memory, and images needed for each job to significantly reduce development time.
                * Supports cloud and self-hosted runners.
                * Developers have SSH access to CI/CD job environments. This feature allows developers to troubleshoot and debug builds on the resources where the build failed, and saves time and frustrations.
                * The CI configuration is done in code in YAML syntax.
                * It provides pre-built Docker support in multiple languages
                * Orbs are reusable pieces of configurations to integrate with third party services like Slack and AWS or to connect your source code repository with CircleCI.
                * Cache dependencies, artifacts, and Docker layers to speed up builds.
                * It accelerates CI/CD pipelines with parallel testing.
                * It has a built-in CLI to use advanced tools easily
                * Comprehensive insights
                * RESTful API allows you to access all information and trigger actions in CircleCI.
                * Webhooks enable developers to create customizable integrations with workflow-level and job-level events.
                * FedRAMP certified and SOC2 Type II compliant
              link_to_documentation: https://circleci.com/docs
          - feature: Secrets Management
            gitlab:
              coverage: 25
              projected_coverage: 50
              description: GitLab provides basic functionality for secret management by allowing secret variables to be set at the project/group levels and limits access.
              details: |
                * CI/CD variables can be added to a project’s settings. Only project members with the Maintainer role can add or update project CI/CD variables. To keep a CI/CD variable secret, put it in the project settings.
                  * Protect variable: If selected, the variable is only available in pipelines that run on protected branches or protected tags.
                  * Mask variable: If selected, the variable’s Value is masked in job logs. The variable fails to save if the value does not meet the masking requirements.
                * Files can be stored securely for use in CI/CD pipelines as “secure files”. These files are stored securely outside of a project’s repository, and are not version controlled. It is safe to store sensitive information in these files. Secure files support both plain text and binary file types.
                * GitLab has built in integration with Vault, however this means that the secrets must be created and managed using Vault.
                * Variable values are encrypted using aes-256-cbc and stored in the database. This data can only be read and decrypted with a valid secrets file.
              improving_product_capabilities: |
                * Increase in Paid GMAU for gitlab.com - by making our [JWT token support OpenIDC](https://gitlab.com/groups/gitlab-org/-/epics/7335) which allows us to integration with additional cloud providers
                * [Extend our lead in CI/CD](https://about.gitlab.com/direction/#extend-our-lead-in-cicd):
                  * [Support non-expanded variables](https://gitlab.com/gitlab-org/gitlab/-/issues/217309) which will solve one of the popular [requests](https://gitlab.com/gitlab-org/gitlab/-/issues/17069) for secrets management
                  * Allow users to [Opt-in JWT token](https://gitlab.com/gitlab-org/gitlab/-/issues/356986) per job which improved our securities around key CI/CD workflows
                * [Support $ sign in environment variables](https://gitlab.com/gitlab-org/gitlab/-/issues/17069)
              link_to_documentation: https://docs.gitlab.com/ee/ci/variables/
            competitor:
              coverage: 25
              description: CircleCI provides basic functionality for secret management using environment variables at the project level, or Contexts for securing and sharing environment variables across projects.
              details: |
                * The UI for the variables is very basic. Includes only name and value but not more features such as protected variables, or files.
                * The variable values are neither readable nor editable in the app after they are set.
                * Secrets masking provides added security within CircleCI by obscuring environment variables in the job output when echo or print is used.
                * Variables can be imported from other projects which can save time by manually copying them.
              link_to_documentation: https://circleci.com/docs/env-vars/#
          - feature: Code Testing and Coverage
            gitlab:
              coverage: 50
              projected_coverage: 50
              description: GitLab offers many tools for visualizing test cases as well as determining coverage and failure.
              details: |
                * Test cases in GitLab can help teams create testing scenarios in their existing development platform.
                * With the help of GitLab CI/CD, users can collect the test coverage information of their favorite testing or coverage-analysis tool, and visualize this information inside the file diff view of merge requests (MRs). This will allow users to see which lines are covered by tests, and which lines still require coverage, before the MR is merged.
                * Users can configure their job to use Unit test reports, and GitLab displays a report on the merge request so that it’s easier and faster to identify the failure without having to check the entire log.
                * Unit test reports currently only support test reports in the JUnit report format.
              improving_product_capabilities: |
                * [Improve aggregation of code coverage across jobs](https://gitlab.com/gitlab-org/gitlab/-/issues/367317)
                * [Add support for Jacoco Coverage Report Parsing](https://gitlab.com/groups/gitlab-org/-/epics/6900)
                * [Historic Test Data for Projects](https://gitlab.com/groups/gitlab-org/-/epics/3129)
              link_to_documentation: https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html
            competitor:
              coverage: 25
              description: CircleCI provides different options for code coverage reporting using built-in CircleCI features combined with open source libraries, or using partners. To view coverage reports you need to store it as an artifact and it will be available in the Job UI.
              details: |
                * CircleCI provides different options for code coverage reporting using built-in CircleCI features combined with open source libraries, or using partners.
                * Code coverage service
                * CodeCov - a partner orb that helps simplify the process of uploading your coverage reports.
                * Coveralls - 3rd party tool
                * Test splitting in which tests are executed simultaneously across multiple execution environments to speed up pipelines.
              link_to_documentation: https://circleci.com/docs/code-coverage/
          - feature: Performance Testing
            gitlab:
              coverage: 25
              projected_coverage: 25
              description: GitLab offers Performance testing for measuring the performance impact of your code changes rendering in the browser as well as under load.
              details: |
                * If an application offers a web interface while using GitLab CI/CD, a user can quickly determine the rendering performance impact of pending code changes in the browser.
                * With Load Performance Testing, a user can test the impact of any pending code changes to an application’s backend in GitLab CI/CD.
                * GitLab uses Sitespeed.io, a free and open source tool, for measuring the rendering performance of web sites.
                * GitLab uses k6, a free and open source tool, for measuring the system performance of applications under load.
              improving_product_capabilities: |
                * [CI View for detailed site speed metrics](https://gitlab.com/gitlab-org/gitlab/issues/9878)
                * [Add link to detailed sitespeed report from Merge Request](https://gitlab.com/gitlab-org/gitlab/issues/9879)
                * [Add integrated load testing to AutoDevOps](https://gitlab.com/gitlab-org/gitlab-ee/issues/10681)
                * [Archive and graph load test results](https://gitlab.com/gitlab-org/gitlab/-/issues/36914)
              link_to_documentation: https://docs.gitlab.com/ee/ci/
            competitor:
              coverage: 0
              description: CircleCI does not offer this.
              details:
              link_to_documentation:
          - feature: Merge Trains
            gitlab:
              coverage: 50
              projected_coverage: 100
              description: GitLab has Merge trains enabled under Premium.
              details: |
                * A merge train is a queued list of merge requests, each waiting to be merged into the target branch.
                * Many merge requests can be added to the train. Each merge request runs its own merged results pipeline, which includes the changes from all of the other merge requests in front of it on the train. All the pipelines run in parallel, to save time. The author of the internal merged result commit is always the user that initiated the merge.
              improving_product_capabilities: |
                * [Merge Trains should support fast forward merge](https://gitlab.com/groups/gitlab-org/-/epics/4911)
                * [API support for merge trains MWPS](https://gitlab.com/gitlab-org/gitlab/-/issues/32665)
                * [Resolving Severity 1 and 2 bugs](https://gitlab.com/gitlab-org/gitlab/-/issues?sort=milestone_due_desc&state=opened&label_name[]=Category:Merge+Trains&label_name[]=type::bug&not[label_name][]=severity::3&not[label_name][]=severity::4)
              link_to_documentation: https://docs.gitlab.com/ee/ci/
            competitor:
              coverage: 25
              description: Users can integrate CircleCI with GitHub and add pull requests to the Merge Queue.
              details:
              link_to_documentation: https://circleci.com/docs/github-integration/#overview
          - feature: Review Apps
            gitlab:
              coverage: 100
              projected_coverage: 100
              description: GitLab allows users to create a new environment for each branch, and view changes in a live deployment.
              details: |
                * Provide an automatic live preview of changes made in a feature branch by spinning up a dynamic environment for merge requests.
                * Allow designers and product managers to see changes without needing to check out a branch and run the changes in a sandbox environment.
                * Are fully integrated with the GitLab DevOps LifeCycle.
                * Allow users to deploy their changes wherever they want.
                * Users can leave comments directly to the MR the review app was started from as well
                * What happens when 2 MRs and deployed to the same review app
                  * Users can always set a variable based off an env value of the environment to avoid collision
                * Works with the Kubernetes auto-integration
              improving_product_capabilities: |
                * [Secure CI_JOB_TOKEN Workflows](https://gitlab.com/groups/gitlab-org/-/epics/6546"}.
                * [Make GitLab CI/CD work better with external repositories](https://gitlab.com/groups/gitlab-org/-/epics/943"}.
              link_to_documentation: https://docs.gitlab.com/ee/ci/
            competitor:
              coverage: 0
              details: |
                * CircleCI offers orbs for many common deployment targets.
              link_to_documentation: https://circleci.com/docs/deployment-overview/
        overview_analysis: |
          CircleCI provides good CI/CD capability on the cloud or self-hosted, with unique capabilities that helps development teams to release code rapidly. The key tools are SSH debugging which allows developers direct access to the job environment for debugging,  resource-class that allows you to configure the runner resources (such as memory and CPU) for each job, and advanced caching and docker layer caching.
          
          CircleCI Orbs are reusable packages of YAML configuration developed by many partners, community or CircleCI. Orbs condenses repeated pieces of config into a single line of code. The Orb registry helps developers search Orbs, similar to the GitHub Marketplace. Orbs enables developers to find many different workflows and integrations from many different vendors, allowing them to quickly resolve their needs.
          
          CircleCI has some features that GitLab is missing such as SSH debugging, and resource class and Orbs/Marketplace.
          
          The big advantage of GitLab over CircleCI is that GitLab is a real DevSecOps platform with many security and compliance scans built-in, enabling development teams work at speed in a single UI, while CircleCI requires teams to use many tools, for planning, for VCS, security, packaging, etc.
        gitlab_product_roadmap:
          - roadmap_item: Make GitLab CI/CD work better with external repositories
          - roadmap_item: Merge Trains will support fast forward merge
          - roadmap_item: Extend our lead in CI/CD with support non-expanded variables
